#!/usr/bin/env python3

import argparse
import logging
import sys

from database import ClassDatabase
from gui      import GUI
from utils    import cleanup

# Disable 'sh' module logger
logging.getLogger('sh').setLevel(logging.WARNING)

# Define our own logger
LOG_LEVEL = logging.DEBUG
logger = logging.getLogger('reds_eda')
formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(filename)s:%(lineno)d - %(message)s')

stream_handler = logging.StreamHandler()
stream_handler.setLevel(LOG_LEVEL)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

logger.setLevel(LOG_LEVEL)

def main():
    parser = argparse.ArgumentParser(description='REDS EDA sharing tool')
    parser.add_argument('--umount', '-u', action='store_true', default=False, help='Unmount all softwares')
    args = parser.parse_args()

    database = ClassDatabase()

    (rc, e) = cleanup(database.softwares)
    if (rc != 0):
        logger.error(e)
    if (rc != 0 or args.umount is True):
        sys.exit(rc)

    gui = GUI(sys.argv, database)
    sys.exit(gui.exec_())

if (__name__ == '__main__'):
    main()
