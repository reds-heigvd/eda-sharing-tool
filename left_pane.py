import logging
import sys

from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import QAbstractButton, QComboBox, QLabel, QMessageBox, QPushButton, QVBoxLayout, QWidget

from database import ClassDatabase
from utils import createLauncher, missingSoftwares, mount

BUTTON_START_TEXT = 'Démarrer'
BUTTON_INIT_TEXT = 'Recherche de mise à jour...'
BUTTON_UPDATE_TEXT = 'Mise à jour disponible'
QUESTION_TEXT = 'Pour quel cours souhaites-tu travailler ?'

logger = logging.getLogger('reds_eda')

class LeftPane(QWidget):
    def __init__(self, database: ClassDatabase, parent=None):
        super().__init__(parent)

        self._database = database
        self._courses = self._database.classes

        self._vpn_state = False

        self.layout = QVBoxLayout(self)

        self.question_label = self._questionLabel()

        self.courses_combo = self._coursesCombobox()

        self.start_button = self._startButton()
        self.start_button.setEnabled(False)
        self.start_button.clicked.connect(self._start)

        self.layout.addStretch(999)
        self.layout.addWidget(self.question_label)
        self.layout.addWidget(self.courses_combo)
        self.layout.addWidget(self.start_button)
        self.layout.addStretch(999)

    def updateFound(self, found: bool):
        if (found is False):
            self.start_button.setText(BUTTON_START_TEXT)
            self.start_button.setEnabled(True)
        else:
            self.start_button.setText(BUTTON_UPDATE_TEXT)
            self.start_button.setEnabled(False)

    def updateVPN(self, state: bool):
        self._vpn_state = state

    def _coursesCombobox(self):
        widget = QComboBox(self)

        widget.addItems(['{}'.format(course) for course in self._courses])

        return widget

    @pyqtSlot(QAbstractButton)
    def _failureExit(self, button: QAbstractButton):   # pylint: disable=unused-argument
        sys.exit(1)

    def _questionLabel(self):
        widget = QLabel(QUESTION_TEXT, parent=self)
        widget.setAlignment(Qt.AlignCenter)

        return widget

    @pyqtSlot()
    def _start(self):
        course = self._courses[self.courses_combo.currentIndex()]
        logger.debug('Starting checks for {}'.format(course))

        need_vpn = False

        missing = missingSoftwares(course)
        if (len(missing) > 0):
            logger.error('Missing softwares: {}'.format([soft.name for soft in missing]))
            str_list = ''.join(['\n- {}'.format(software.name) for software in missing])
            dialog = QMessageBox(QMessageBox.Critical,
                'Erreur',
                'Les disques suivants sont manquants :{}'.format(str_list),
                buttons=QMessageBox.Ok
            )
            # dialog.buttonClicked.connect(self._failureExit)
            dialog.exec()
            return

        for soft in course.softs:
            (rc, e) = mount(soft)
            if (rc != 0):
                logger.error(e)
                dialog = QMessageBox(QMessageBox.Critical,
                    'Erreur',
                    'Erreur lors du montage de {}:\n{}'.format(soft.name, e),
                    buttons=QMessageBox.Ok
                )
                # dialog.buttonClicked.connect(self._failureExit)
                dialog.exec()
                return

            if (soft.desktop is True):
                (rc, e) = createLauncher(soft)
                if (rc != 0):
                    logger.debug('createLauncher(): {}'.format(e))

            if (soft.vpn is True and need_vpn is False):
                need_vpn = True

        if (need_vpn is True and self._vpn_state is False):
            dialog = QMessageBox(QMessageBox.Warning,
                'Attention',
                'Attention, vous n\'êtes pas connecté(e) au VPN. Certains logiciels pourraient ne pas fonctionner \
                    correctement.',
                buttons=QMessageBox.Ok
            )
            dialog.exec()

        dialog = QMessageBox(QMessageBox.NoIcon,
            'Succès',
            'Bon travail !',
            buttons=QMessageBox.Ok
        )
        dialog.buttonClicked.connect(self._successExit)
        dialog.exec()
        return

    def _startButton(self):
        widget = QPushButton(BUTTON_INIT_TEXT)

        return widget

    @pyqtSlot(QAbstractButton)
    def _successExit(self, button: QAbstractButton):   # pylint: disable=unused-argument
        sys.exit(0)
