# REDS EDA software mounting tool

Since the world is currently going through a pandemic, students don't have access to some EDA software. A solution to
counter this issue has been developed at the REDS Institute of the HEIG-VD school.

This solution consists of sharing Virtual Disk Images (VDIs) with students. Each VDI contains a software. These VDIs
can be downloaded from the `reds-data` server.


## Installing dependencies

First of all, make sure that Python 3 and its package manager `pip` are installed. On Debian-based systems, you can
install those packages via the `apt` package manager:

```
# apt update
# apt install python3 python3-pip
```

Then, install the needed Python modules by issuing the following command:

```
$ pip3 install -r requirements.txt
```


## Running

To run the application, simply execute:

```
python3 main.py
```
