import logging

import os
from os.path import exists, ismount
from typing import List, Tuple

from sh import ln, rm, sudo, ErrorReturnCode

from database import Class, Software

logger = logging.getLogger('reds_eda')


def cleanup(softs: List[Software]) -> Tuple[int, ErrorReturnCode]:
    rc = 0

    # Kill JTAG server
    try:
        sudo.killall('-9', 'jtagd', '-q')
    except ErrorReturnCode as e:
        pass

    for software in softs:
        logger.debug('{}: {}'.format(software.name, software.disk.mountPath))
        if (exists(software.disk.mountPath)):
            logger.debug('  -> mountPath exists')
            if (ismount(software.disk.mountPath)):
                logger.debug('    -> mountPath is a mount point')
                try:
                    rc = sudo.umount(software.disk.mountPath).exit_code
                except ErrorReturnCode as e:
                    return (rc, e)
                finally:
                    logger.debug('      unmount() returned {}'.format(rc))

            try:
                rc = sudo.rmdir(software.disk.mountPath).exit_code
            except ErrorReturnCode as e:
                return (rc, e)
            finally:
                logger.debug('    rmdir() returned {}'.format(rc))

        if (software.desktop is True):
            home = os.environ.copy()['HOME']
            name = 'reds-{}'.format(software.name.replace(' ', '_').lower())
            path = '{}/Desktop/{}.desktop'.format(home, name)

            if (exists(path)):
                logger.debug('  -> Desktop file exists')

                try:
                    rc = rm(path).exit_code
                except ErrorReturnCode as e:
                    return (rc, e)
                finally:
                    logger.debug('    rm() returned {}'.format(rc))


        logger.debug('  -> cleanup OK')

    return (0, None)


def createLauncher(soft: Software):
    rc = 0

    if (soft.desktop is False):
        return (0, None)

    home = os.environ.copy()['HOME']
    name = 'reds-{}'.format(soft.name.replace(' ', '_').lower())

    try:
        rc = ln('-s', '{}/.local/share/applications/{}.desktop'.format(home, name), '{}/Desktop/{}.desktop'.format(home, name)).exit_code
    except ErrorReturnCode as e:
        return (rc, e)
    else:
        return (0, None)


def missingSoftwares(course: Class):
    return list(filter(lambda soft: soft.disk.device is None, course.softs))


def mount(soft: Software):
    rc = 0

    try:
        rc = sudo.mkdir(soft.disk.mountPath, '-p')
        rc = sudo.mount(soft.disk.device, soft.disk.mountPath, t='ext4').exit_code
    except ErrorReturnCode as e:
        return (rc, e)
    else:
        return (0, None)
