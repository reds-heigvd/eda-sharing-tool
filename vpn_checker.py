import logging
import subprocess

from PyQt5.QtCore import QObject, QTimer, pyqtSignal

logger = logging.getLogger('reds_eda')

PING_HOST = 'eilic01.einet.ad.eivd.ch'

class VPNChecker(QObject):
    stateChanged = pyqtSignal(bool)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.connected = False

        self.timer = QTimer()
        self.timer.timeout.connect(self.checkConnection)
        self.timer.start(1000)

    def checkConnection(self):
        command = ['ping', '-c', '1', PING_HOST]

        response = False
        try:
            response = (subprocess.call(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=0.2) == 0)
        except subprocess.TimeoutExpired:
            response = False

        if (response != self.connected):
            logger.debug('VPN state changed: {}'.format('CONNECTED' if (response is True) else 'DISCONNECTED'))
            self.connected = response
            self.stateChanged.emit(self.connected)
