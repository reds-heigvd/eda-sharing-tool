import logging

from os import listdir, readlink
from os.path import dirname, islink, join, realpath
from typing import List

logger = logging.getLogger('reds_eda')


DEV_DISK_UUID_PATH  = '/dev/disk/by-uuid'


class Disk():
    def __init__(self, uuid: str, mount_path: str):
        self._uuid = uuid
        self._mount_path = mount_path
        self._device = self._retrieveDevice()

    @property
    def device(self) -> str:
        return self._device

    @property
    def mountPath(self) -> str:
        return self._mount_path

    @property
    def uuid(self) -> str:
        return self._uuid

    def _retrieveDevice(self) -> str:
        for uuid in listdir(DEV_DISK_UUID_PATH):
            path = join(DEV_DISK_UUID_PATH, uuid)
            if (islink(path) and (uuid == self._uuid)):
                return realpath(join(dirname(path), readlink(path)))

        return None


class Software():
    def __init__(self, name: str, disk: Disk, vpn: bool=False, desktop: bool=False):
        self._name = name
        self._disk = disk
        self._vpn = vpn
        self._desktop = desktop

    @property
    def desktop(self) -> bool:
        return self._desktop

    @property
    def disk(self) -> Disk:
        return self._disk

    @property
    def name(self) -> str:
        return self._name

    @property
    def vpn(self) -> bool:
        return self._vpn


class SoftwareDatabase():
    def __init__(self):
        self._softs: List[Software] = []

        # Populate softwares database
        # CodeComposer Studio 10
        self.ccstudio_10 = Software(
            'Code Composer Studio 10.2',
            Disk(
                'a131c11b-a289-4107-9e2c-2e7159f253f8',
                '/opt/ccs'
            ),
            desktop=True
        )
        self._softs.append(self.ccstudio_10)

        # Quartus 13.0
        self.quartus_13 = Software(
            'Quartus 13.0',
            Disk(
                '497f11c5-1488-4209-8835-0d9fea8e7719',
                '/opt/intelFPGA/13.0'
            ),
            vpn=True
        )
        self._softs.append(self.quartus_13)

        # Quartus 18.1
        self.quartus_18 = Software(
            'Quartus 18.1',
            Disk(
                '7b19d8e7-f5cb-4a79-83bc-a21c3d8d4fa7',
                '/opt/intelFPGA/18.1'
            ),
            vpn=True,
            desktop=True
        )
        self._softs.append(self.quartus_18)

        # QuestaSim/QuestaFormal 2020
        self.questa_2020 = Software(
            'Questa 2020',
            Disk(
                '6d742d88-e57e-492f-9180-e6700fc808fb',
                '/opt/mentor'
            ),
            vpn=True
        )
        self._softs.append(self.questa_2020)

        # Xilinx ISE 14.7
        self.xilinx_ISE = Software(
            'Xilinx ISE 14.7',
            Disk(
                '98009814-9c96-44cb-8e88-820b098f1287',
                '/opt/Xilinx/ISE'
            ),
            vpn=True
        )
        self._softs.append(self.xilinx_ISE)

        # For debug purposes
        for soft in self._softs:
            logger.debug('{}: {}'.format(soft.name, soft.disk.device if (soft.disk.device is not None) else 'missing'))

    @property
    def availableSoftwares(self):
        # Return only softwares for which a device has been found
        return list(filter(lambda soft: soft.disk.device is not None, self._softs))

    @property
    def softwares(self):
        return self._softs.copy()


class Class():
    def __init__(self, acronym: str, name: str, softs: List[Software]=None):
        self._acronym = acronym
        self._name = name
        self._softs = softs if (softs is not None) else []

    @property
    def acronym(self) -> str:
        return self._acronym

    @property
    def name(self) -> str:
        return self._name

    @property
    def softs(self) -> List[Software]:
        return None if (self._softs is None) else self._softs.copy()

    def __str__(self):
        return self._name if (self._acronym is None) else '[{}] {}'.format(self._acronym, self._name)


class ClassDatabase():
    def __init__(self):
        self._softs = SoftwareDatabase()

        # Populate classes database
        self._classes: List[Class] = [
            Class(None, 'Autre'),
            Class('ASP',     'Architecture des Systèmes à Processeur', [self._softs.ccstudio_10]),
            Class('CSF',     'Conception de Systèmes numériques sur FPGA', [self._softs.quartus_18, self._softs.questa_2020]),
            Class('IFS',     'Interfaces', [self._softs.xilinx_ISE]),
            Class('SOCF',    'System-On-Chip on FPGA', [self._softs.quartus_18, self._softs.questa_2020]),
            Class('Syslog2', 'Systèmes Logiques 2', [self._softs.quartus_18, self._softs.questa_2020]),
            Class('VSN',     'Vérification de Systèmes Numériques', [self._softs.questa_2020])
        ]

    @property
    def classes(self) -> List[Class]:
        return self._classes.copy()

    @property
    def softwares(self) -> List[Software]:
        return self._softs.softwares
