#!/bin/bash

CWD=/home/reds/.reds/eda-tool
LOG_FILE=/var/log/reds/eda-tool.log
PYTHON=/usr/bin/python3

RETRIEVE_LAB_DIR=/etc/reds

# Redirect every log to LOG_FILE: https://serverfault.com/a/103569
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>${LOG_FILE} 2>&1

# This function returns 0 if we have access to internet, 1 otherwise
is_connected()
{
    nc -z 8.8.8.8 53 -w 3 > /dev/null
}

# This function forces a clean update of the local repository
force_update()
{
    cd $CWD
    git switch master
    git clean -fd
    git reset --hard
    git pull origin master
}

# This function updates the retrieve_lab.sh script available in $PATH
update_retrieve_lab()
{
    sudo cp ${CWD}/retrieve_lab.sh ${RETRIEVE_LAB_DIR}/retrieve_lab.sh
    sudo chmod +x ${RETRIEVE_LAB_DIR}/retrieve_lab.sh
}

echo "=== REPOSITORY UPDATE ==="
if [[ is_connected ]]; then
    force_update
    update_retrieve_lab
else
    echo "WARNING: No internet connection available"
fi

echo "=== STARTING EDA-TOOL ==="
${PYTHON} ${CWD}/main.py $1
