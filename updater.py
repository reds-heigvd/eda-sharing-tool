import logging
import os
import sys
import traceback

import git

from PyQt5.QtCore import QObject, QRunnable, QThreadPool, pyqtSignal, pyqtSlot

logger = logging.getLogger('reds_eda')

# REPO_URI = 'https://gitlab.com/reds-heigvd/eda-sharing-tool.git'
REPO_ABSPATH = os.path.dirname(os.path.abspath(__file__))

class _ThreadSignals(QObject):
    done = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(bool)


class _Thread(QRunnable):    # pylint: disable=too-few-public-methods
    def __init__(self, fn, *args, **kwargs):
        super().__init__()

        self.fn = fn
        self.args = args
        self.kwargs = kwargs

        self.signals = _ThreadSignals()

    def run(self):
        try:
            result = self.fn(*self.args, **self.kwargs)
        except: # pylint: disable=bare-except
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)
        finally:
            self.signals.done.emit()


class Updater(QObject):
    updateFound = pyqtSignal(bool)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.repo = git.Repo(REPO_ABSPATH)

        logger.debug('Repository absolute path: {}'.format(REPO_ABSPATH))

        self.pool = QThreadPool()
        self.thread = None

    def restart(self):
        python = sys.executable
        logger.debug('Restarting application ({} {})'.format(python, sys.argv))
        os.execl(python, python, * sys.argv)

    def updateAvailable(self) -> bool:
        available = False
        try:
            local_commit = self.repo.head.commit
            remote_commit = self.repo.remotes.origin.fetch()[0].commit

            available = (local_commit != remote_commit)

            if (available is True):
                logger.debug('An update is available')
            else:
                logger.debug('Up-to-date')
        except git.GitError as e:
            logger.error(e)
            available = False
        finally:
            return available

    def updateAvailableAsync(self):
        if (self.thread is not None):
            logger.warning('Check for an update is already running')
            return

        logger.debug('Checking for update asynchronously...')

        self.thread = _Thread(self.updateAvailable)
        self.thread.signals.done.connect(self._threadDone)
        self.thread.signals.result.connect(self.updateFound.emit)
        self.pool.start(self.thread)

    def update(self) -> bool:
        try:
            self.repo.remotes.origin.pull()
            logger.debug('Update successful')
            return True
        except git.GitError as e:
            logger.error(e)
            return False

    @pyqtSlot()
    def _threadDone(self):
        self.pool.waitForDone(-1)
        self.thread = None

    @pyqtSlot(tuple)
    def _threadError(self, error: tuple):
        exctype, value, trace = error
        logger.error('type: {}, value: {}, traceback: {}'.format(exctype, value, trace))
