import logging
import sys

from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import (
    QApplication, QAbstractButton, QHBoxLayout, QMainWindow, QMessageBox, QSizePolicy, QSplitter, QWidget
)

from database import ClassDatabase
from left_pane import LeftPane
from right_pane import RightPane
from updater import Updater
from vpn_checker import VPNChecker

logger = logging.getLogger('reds_eda')

WINDOW_TITLE = 'REDS EDA tool'


class GUI(QApplication):
    def __init__(self, argv, database: ClassDatabase):
        super().__init__(argv)

        self.main_window = MainWindow(database)

    def exec_(self):
        self.main_window.show()
        super().exec_()


class MainWindow(QMainWindow):
    def __init__(self, database: ClassDatabase, parent=None):
        super().__init__(parent)

        self.setWindowTitle(WINDOW_TITLE)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.updater = Updater()
        self.vpn_checker = VPNChecker()

        self.classDb = database

        self.main_widget = QWidget(self)
        self.main_layout = QHBoxLayout(self.main_widget)

        self.splitter = QSplitter(Qt.Horizontal, self.main_widget)

        self.left_pane = LeftPane(self.classDb, self.splitter)
        self.right_pane = RightPane(self.splitter)

        self.splitter.addWidget(self.left_pane)
        self.splitter.addWidget(self.right_pane)

        self.splitter.setHandleWidth(20)
        self.splitter.setStretchFactor(0, 999)
        self.splitter.setChildrenCollapsible(False)

        self.main_layout.addWidget(self.splitter)
        self.setCentralWidget(self.main_widget)

        # Connect signals and slots
        self.updater.updateFound.connect(self._updateFound)
        self.vpn_checker.stateChanged.connect(self._stateChanged)

        # Asynchronously check for an update
        self.updater.updateAvailableAsync()

    @pyqtSlot(QAbstractButton)
    def _buttonClicked(self, button: QAbstractButton):  # pylint: disable=unused-argument
        if (self.updater.update() is True):
            self.updater.restart()
        else:
            dialog = QMessageBox(QMessageBox.Critical,
                'Erreur',
                'Une erreur est survenue pendant la mise à jour. Veuillez contacter un assistant de cours.',
                buttons=QMessageBox.Ok
            )
            dialog.buttonClicked.connect(self._quit)
            dialog.exec()

    @pyqtSlot(bool)
    def _stateChanged(self, state: bool):
        self.right_pane.updateVPN(state)
        self.left_pane.updateVPN(state)

    @pyqtSlot(QAbstractButton)
    def _quit(self, button: QAbstractButton):   # pylint: disable=unused-argument
        sys.exit(1)

    @pyqtSlot(bool)
    def _updateFound(self, found: bool):
        self.left_pane.updateFound(found)
        self.right_pane.updateFound(found)

        if (found is True):
            dialog = QMessageBox(QMessageBox.Warning,
                'Mise à jour disponible',
                'Une version plus récente est disponible. Le programme redémarrera après la mise à jour.',
                buttons=QMessageBox.Ok
            )
            dialog.buttonClicked.connect(self._buttonClicked)
            dialog.exec()
