import logging
import os

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel, QVBoxLayout, QWidget

logger = logging.getLogger('reds_eda')

UPDATE_LABEL_AVAILABLE_TEXT = 'Une mise à jour est disponible'
UPDATE_LABEL_CHECKING_TEXT = 'Recherche de mise à jour...'
UPDATE_LABEL_UPTODATE_TEXT = 'Logiciel à jour'

VPN_LABEL_CONNECTED_TEXT = 'Connecté(e) au VPN'
VPN_LABEL_DISCONNECTED_TEXT = 'Non connecté(e) au VPN'

FAILURE_COLOR = 'red'
SUCCESS_COLOR = 'green'

class RightPane(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.layout = QVBoxLayout(self)

        self.logo = QPixmap(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logo.png'))
        self.logo = self.logo.scaledToWidth(180)

        self.logo_label = QLabel()
        self.logo_label.setPixmap(self.logo)

        self.update_label = QLabel(UPDATE_LABEL_CHECKING_TEXT)
        self.update_label.setStyleSheet('color: {}'.format(FAILURE_COLOR))

        self.vpn_label = QLabel(VPN_LABEL_DISCONNECTED_TEXT)
        self.vpn_label.setStyleSheet('color: {}'.format(FAILURE_COLOR))

        self.layout.addWidget(self.logo_label)
        self.layout.addSpacing(40)
        self.layout.addWidget(self.update_label)
        self.layout.addWidget(self.vpn_label)
        self.layout.addStretch(999)

    @pyqtSlot(bool)
    def updateFound(self, found: bool):
        if (found is False):
            self.update_label.setText(UPDATE_LABEL_UPTODATE_TEXT)
            self.update_label.setStyleSheet('color: {}'.format(SUCCESS_COLOR))
        else:
            self.update_label.setText(UPDATE_LABEL_AVAILABLE_TEXT)
            self.update_label.setStyleSheet('color: {}'.format(FAILURE_COLOR))

    @pyqtSlot(bool)
    def updateVPN(self, connected: bool):
        if (connected is False):
            self.vpn_label.setText(VPN_LABEL_DISCONNECTED_TEXT)
            self.vpn_label.setStyleSheet('color: {}'.format(FAILURE_COLOR))
        else:
            self.vpn_label.setText(VPN_LABEL_CONNECTED_TEXT)
            self.vpn_label.setStyleSheet('color: {}'.format(SUCCESS_COLOR))
